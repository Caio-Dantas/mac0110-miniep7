#MAC0110 - MiniEP7
#Caio Dantas Simão Ugêda - 11796868


#----------Funções da Parte 2

#Função para comparar valores com determinada margem de erro
function quaseigual(v1, v2)
  erro = 0.001
  igual = abs(v1 - v2)
  if igual <= erro
    return true
  end
  return false
end

#Função para validar determinado valor de seno de x
function check_sin(value, x)
  return quaseigual(value, taylor_sin(x))
end

#Função para validar determinado valor de cosseno de x
function check_cos(value, x)
  return quaseigual(value, taylor_cos(x))
end

#Função para validar determinado valor de tangente de x
function check_tan(value, x)
  return quaseigual(value, taylor_tan(x))
end



#------Funções da Parte  1 / 3


#Função auxiliar para o cálculo da tangente de x
function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
      A[m + 1] = 1 // (m + 1)
          for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
          end
      end
      return abs(A[1])
end

#Função para calcular o seno de x
function taylor_sin(x)
 seno = 0
 indice = 1
 paridade = 1
    for i in  1:10
      seno += ((x^indice)/factorial(indice)*paridade)
      paridade *= -1
      indice += 2
    end
    return seno
end

#Função para calcular o cosseno de x
function taylor_cos(x)
   cosseno = 0
   indice = 0
   paridade = 1
      for i in  1:10
        cosseno += ((x^indice)/factorial(indice)*paridade)
        paridade *= -1
        indice += 2
      end
      return cosseno
end


#Função para o cálculo de tangente de x
function taylor_tan(x)
  t = 0
  n = BigInt(1)
  for i in 1:10
    t += ((2^(2*n)) * ((2^(2*n))-1) * bernoulli(n) * x^((2*n)-1))/factorial(2*n)
    n = n+1
  end
  return t
end


#------ Parte 3

#Testes
using Test
function test()

  #Teste da função de seno
  @test check_sin(taylor_sin(pi/4), pi/4)
  @test check_sin(taylor_sin(pi/2), pi/2)
  @test check_sin(taylor_sin(pi/3), pi/3)
  @test check_sin(taylor_sin(pi), pi)
  print("seno(x): OK \n")

  #Teste da função de cosseno
  @test check_cos(taylor_cos(pi/4), pi/4)
  @test check_cos(taylor_cos(pi/3), pi/3)
  @test check_cos(taylor_cos(pi/2), pi/2)
  @test check_cos(taylor_cos(pi), pi)
  print("cosseno(x): OK \n")

  #Teste da função de tangente
  @test check_tan(taylor_tan(pi/4), pi/4)
  @test check_tan(taylor_tan(pi/3), pi/3)
  @test check_tan(taylor_tan(pi/6), pi/6)
  @test !check_tan(taylor_tan(pi), pi)
  print("tangente(x): OK \n")

end
